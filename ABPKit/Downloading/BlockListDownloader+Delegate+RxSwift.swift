/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

extension BlockListDownloader
{
    /// A URL session task is transferring data.
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                    totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64)
    {
        let taskID = downloadTask.taskIdentifier
        downloadEvents[taskID]?.onNext(
            DownloadEvent(
                withNotFinishedEvent: lastDownloadEvent(taskID: taskID),
                bytesWritten: totalBytesWritten))
    }

    /// A URL session task has finished transferring data.
    /// Download events are updated.
    /// The downloaded data is persisted to local storage.
    func urlSession(_ session: URLSession,
                    task: URLSessionTask,
                    didCompleteWithError error: Error?)
    {
        let taskID = task.taskIdentifier
        downloadEvents[taskID]?.onNext(DownloadEvent(finishWithEvent: lastDownloadEvent(taskID: taskID)))
        if error != nil { reportError(error!, taskID: taskID) }
        downloadEvents[taskID]?.onCompleted()
    }

    /// Report an error.
    func reportError(_ error: Error, taskID: DownloadTaskID)
    {
        downloadEvents[taskID]?.onError(error)
    }
}
