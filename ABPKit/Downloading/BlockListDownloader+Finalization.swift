/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

extension BlockListDownloader
{
    /// Data for download finalization.
    struct MoveItemData
    {
        let downloadTask: URLSessionDownloadTask,
            source: URL,
            destination: URL,
            filename: String,
            taskID: Int,
            taskIndex: Int,
            dlVersion: (URLResponse?) -> String
    }

    /// Downloads are added for a User.
    func finalizeDownload<S>(downloadTask: URLSessionDownloadTask, location: URL, filename: String) -> (S) throws -> Void
    where S: BlockListDownloadable & Persistable
    {
        { consumer in
            let taskID = downloadTask.taskIdentifier
            guard let idx = self.indexForTaskID()(taskID) else {
                self.reportError(ABPDownloadTaskError.badSourceDownload, taskID: taskID); return
            }
            /// A version from the server. It is not used for download date, by design.
            let dlVersion: (URLResponse?) -> String = {
                let dateKey = "Date"
                let dflt = Date().asTimestamp() // default value
                if let resp = $0 as? HTTPURLResponse {
                    return (resp.allHeaderFields[dateKey] as? Date)?.asTimestamp() ?? dflt
                }
                return dflt
            }
            let destination: () throws -> URL = { try Config().containerURL().appendingPathComponent(filename, isDirectory: false) }
            self.moveItemAction()(try MoveItemData(
                downloadTask: downloadTask,
                source: location,
                destination: destination(),
                filename: filename,
                taskID: taskID,
                taskIndex: idx,
                dlVersion: dlVersion))
        }
    }

    // Final state update after moving download.
    func moveItemComplete() -> (MoveItemData) -> Void
    {
        { dat in
            switch self.srcDownloads[dat.taskIndex].initiator {
            case .userAction:
                if let srcBL = self.srcDownloads[dat.taskIndex].blockList.map({ $0 }) {
                    // Only AA enableable sources should succeed:
                    do {
                        self.user = try self.lastVersionSet(dat.dlVersion(dat.downloadTask.response))(self.user)
                            .downloadAdded()(try self.blockListForPersistence(date: Date())(srcBL))
                    } catch let err { self.reportError(err, taskID: dat.taskID) }
                } else { self.reportError(ABPDownloadTaskError.badSourceDownload, taskID: dat.taskID) }
            case .automaticUpdate:
                if let srcBL = self.srcDownloads[dat.taskIndex].blockList.map({ $0 }) {
                    // Only AA enableable sources should succeed:
                    do {
                        self.updater = try self.lastVersionSet(dat.dlVersion(dat.downloadTask.response))(self.updater)
                            .downloadAdded()(try self.blockListForPersistence(date: Date())(srcBL))
                    } catch let err { self.reportError(err, taskID: dat.taskID) }
                } else { self.reportError(ABPDownloadTaskError.badSourceDownload, taskID: dat.taskID) }
            default:
                self.reportError(ABPBlockListError.badInitiator, taskID: dat.taskID)
            }
        }
    }
}
