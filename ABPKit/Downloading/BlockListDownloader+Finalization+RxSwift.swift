/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

import RxSwift

extension BlockListDownloader
{
    /// Handle moving a download to its final destination.
    func moveItemAction() -> (MoveItemData) -> Void
    {
        { dat in
            self.moveItem(
                    source: dat.source,
                    destination: dat.destination)
                .subscribe { cmpl in
                    switch cmpl {
                    case .completed:
                        self.moveItemComplete()(dat)
                    case .error(let err):
                        self.reportError(err, taskID: dat.taskID)
                    }
                }.disposed(by: Bags.bag()(.blockListDownloader, self))
        }
    }

    /// Move a file to a destination. If the file exists, it will be first removed, if possible. If
    /// the operation cannot be completed, an error will be thrown.
    private
    func moveItem(source: URL, destination: URL?) -> Completable
    {
        guard let dst = destination else { return .error(ABPDownloadTaskError.badDestinationURL) }
        let mgr = FileManager.default
        do {
            try mgr.moveItem(at: source, to: dst)
            return .empty()
        } catch let err { return .error(err) }
    }
}
