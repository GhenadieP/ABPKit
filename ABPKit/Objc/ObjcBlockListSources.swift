/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

// It is needed to define a Objective-C translatable Swift layer that will expose
// the current functionality to Objective-C clients. That is - it was decided
// to create a translatable swift layer rather than modifying existing swift code to
// be Objective-C compatible.
//
// Objc BlockListSource is defined as simple enum with raw type Int, so it can be easily used
// in Objc code, but as soon it needs to be passed deeper into the framework it will be transformed
// to BlockListSourceable type, thus this is used just for conversion and does not contain any logic.

/// Available block list sources to be used as a source for particular block list
@objc(BlockListSource)
public
enum ObjcBlockListSource: Int {
    case bundledEasylist
    case bundledEasylistPlusExceptions
    case remoteEasylist
    case remoteEasylistPlusExceptions
}

// MARK: - Objc API <-> Swift conversion -

extension ObjcBlockListSource {

    /// Will convert the given BlockListSourceable to an objc representable enum
    init(sourceable: BlockListSourceable) {
        switch sourceable {
        case let (remote as RemoteBlockList):
            switch remote {
            case .easylist:
                self = .remoteEasylist
            case .easylistPlusExceptions:
                self = .remoteEasylistPlusExceptions
            }
        case let (bundled as BundledBlockList):
            switch bundled {
            case .easylist:
                self = .bundledEasylist
            case .easylistPlusExceptions:
                self = .bundledEasylistPlusExceptions
            }
        default:
            self = .remoteEasylistPlusExceptions
        }
    }

    /// Converts objc representable enum to BlockListSourceable
    var toBlockListSourceable: BlockListSourceable {
        switch self {
        case .bundledEasylist:
            return BundledBlockList.easylist
        case .bundledEasylistPlusExceptions:
            return BundledBlockList.easylistPlusExceptions
        case .remoteEasylist:
            return RemoteBlockList.easylist
        case .remoteEasylistPlusExceptions:
            return RemoteBlockList.easylistPlusExceptions
        }
    }
}
