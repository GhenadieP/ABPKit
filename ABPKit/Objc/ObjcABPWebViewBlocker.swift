/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

// A thin Objective-C convertible API
@available(iOS 11.0, macOS 10.13, *)
extension ABPWebViewBlocker {
    @objc(initWithHost:error:)
    public
    convenience init(host: ABPBlockable) throws {
        try self.init(host: host,
                      user: nil,
                      noRemote: false,
                      logFileRemovals: false)
    }

    /// Uses a given user state.
    @objc(initWithHost:user:error:)
    public
    convenience init(host: ABPBlockable, user objcUser: ObjcUser) throws {
        try self.init(host: host,
                      user: objcUser.toUser,
                      noRemote: false,
                      logFileRemovals: false)
    }

    @objc(initWithHost:noRemote:error:)
    public
    convenience init(host: ABPBlockable, noRemote: Bool) throws {
        try self.init(host: host,
                      user: nil,
                      noRemote: noRemote,
                      logFileRemovals: false)
    }

    @objc(initWithHost:logFileRemovals:error:)
    public
    convenience init(host: ABPBlockable, logFileRemovals: Bool) throws {
        try self.init(host: host,
                      user: nil,
                      noRemote: false,
                      logFileRemovals: logFileRemovals)
    }

    /// Uses a given user state.
    @objc(initWithHost:user:noRemote:error:)
    public
    convenience init(host: ABPBlockable, user objcUser: ObjcUser, noRemote: Bool) throws {
        try self.init(host: host,
                      user: objcUser.toUser,
                      noRemote: noRemote,
                      logFileRemovals: false)
    }

    /// Uses a given user state.
    @objc(initWithHost:user:logFileRemovals:error:)
    public
    convenience init(host: ABPBlockable, user objcUser: ObjcUser, logFileRemovals: Bool) throws {
        try self.init(host: host,
                      user: objcUser.toUser,
                      noRemote: false,
                      logFileRemovals: logFileRemovals)
    }

    /// Uses a given user state.
    @objc(initWithHost:user:noRemote:logFileRemovals:error:)
    public
    convenience init(host: ABPBlockable, user objcUser: ObjcUser, noRemote: Bool, logFileRemovals: Bool) throws {
        try self.init(host: host,
                      user: objcUser.toUser,
                      noRemote: noRemote,
                      logFileRemovals: logFileRemovals)
    }

    /// Retrieve the last user persisted state. If none, returns the default.
    @objc(lastPersistedUserWithError:)
    public
    func objcLastPersistedUser() throws -> ObjcUser {
        try lastUser().toObjcUser
    }

    /// Overrideable function allowing specification of the user state that will be used for block list updates.
    @objc(setUserForBlockListUpdate:)
    public
    func setUserForBlockListUpdate(_ objcUser: ObjcUser) {
        setUserForBlockListUpdate { objcUser.toUser }
    }

    /// Sets the current active user
    public
    func setUser(_ objcUser: ObjcUser) {
        user = objcUser.toUser
    }

    /// Gets the current active user
    public
    func getUser() -> ObjcUser {
        user.toObjcUser
    }

    /// For extra performance, rules are not validated or counted before loading.
    /// This function provides that service.
    /// - parameter completion: Callback that gives a struct containing rules validation results.
    public
    func validateRules(user objcUser: ObjcUser,
                       completion: @escaping (ObjcRulesValidation?, Error?) -> Void) {
        validateRules(user: objcUser.toUser) { result in
            switch result {
            case let .success(validation):
                completion(.init(validation), nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    // A lightweight objective-c counterpart since objective-c can't have function
    // parameters with default values.
    /// Activate bundled or downloaded rule list, as needed.
    /// * Requires a persisted User to be available.
    public func useContentBlocking(completion: @escaping (Error?) -> Void) {
        useContentBlocking(completeWith: completion)
    }
}

/// For checking rules.
/// * When rules cannot be loaded into the WK store.
@objcMembers
@objc(RulesValidation)
public
class ObjcRulesValidation: NSObject {
    public let parseSucceeded: Bool
    public let rulesCounted: NSNumber?
    public let error: Error?

    fileprivate init(_ rulesValidation: RulesValidation) {
        self.parseSucceeded = rulesValidation.parseSucceeded
        self.rulesCounted = rulesValidation.rulesCounted.map(NSNumber.init)
        self.error = rulesValidation.error
    }
}
