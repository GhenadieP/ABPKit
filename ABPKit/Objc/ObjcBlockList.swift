/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

// It is needed to define a Objective-C translatable Swift layer that will expose
// the current functionality to Objective-C clients. That is - it was decided
// to create a translatable swift layer rather than modifying existing swift code to
// be Objective-C compatible.
//
// ObjcBlockList is a lightweight Objective-C convertible representation of BlockList to
// be used to allow Objective-C clients to create new block lists. It is used just for conversion.
// All of the BlockList related logic still lives in BlockList; ObjcBlockList will just forward it
// if needed.

/// Represents content blocking lists for WKWebView and Safari.
/// Saved rules are named after the BlockList's name.
@objcMembers
@objc(BlockList)
public
class ObjcBlockList: NSObject {
    /// Identifier.
    public let name: String
    /// The source of a the blocklist
    public let source: ObjcBlockListSource
    public let dateDownload: Date?
    public let initiator: ObjcDownloadInitiator

    fileprivate
    init(blockList: BlockList) {
        self.name = blockList.name
        self.source = .init(sourceable: blockList.source)
        self.dateDownload = blockList.dateDownload
        self.initiator = .init(from: blockList.initiator)
    }

    public override func isEqual(_ object: Any?) -> Bool {
        guard let otherList = object as? ObjcBlockList else {
            return false
        }

        return name == otherList.name &&
            source == otherList.source &&
            dateDownload == otherList.dateDownload &&
            initiator == otherList.initiator
    }
}

// MARK: - Factory -

extension ObjcBlockList {
    @objc(makeInstanceWithAcceptableAds:source:name:dateDownload:initiator:error:)
    public
    static func makeInstance(withAcceptableAds: Bool,
                             source: ObjcBlockListSource,
                             name: String? = nil,
                             dateDownload: Date? = nil,
                             initiator: ObjcDownloadInitiator) throws -> ObjcBlockList {
        try BlockList(withAcceptableAds: withAcceptableAds,
                      source: source.toBlockListSourceable,
                      name: name,
                      dateDownload: dateDownload,
                      initiator: initiator.toDownloadInitiator).toObjcBlockList
    }

    @objc(makeInstanceWithAcceptableAds:source:initiator:error:)
    public
    static func makeInstance(withAcceptableAds: Bool,
                             source: ObjcBlockListSource,
                             initiator: ObjcDownloadInitiator) throws -> ObjcBlockList {
        try BlockList(withAcceptableAds: withAcceptableAds,
                      source: source.toBlockListSourceable,
                      initiator: initiator.toDownloadInitiator).toObjcBlockList
    }
}

// MARK: - Objc <-> Swift Mapping -

extension ObjcBlockList {
    var toBlockList: BlockList {
        BlockList(objcBlockList: self)
    }

    /// Determine if a block list is expired or not.
    /// - returns: True if considered expired.
    public
    func isExpired() -> Bool {
        // To avoid duplicating the logic, we rather transform to
        // to base block list and use the logic defined there
        toBlockList.isExpired(debug: false)
    }

    /// Determine if a block list is expired or not.
    /// Debug block list expiration value is used.
    /// 
    /// - returns: True if considered expired.
    public
    func isExpiredDebug() -> Bool {
        // To avoid duplicating the logic, we rather transform to
        // to base block list and use the logic defined there
        toBlockList.isExpired(debug: true)
    }
}

extension Array where Element == ObjcBlockList {
    var toBlockLists: [BlockList] {
        map { $0.toBlockList }
    }
}

extension BlockList {
    var toObjcBlockList: ObjcBlockList {
        ObjcBlockList(blockList: self)
    }

    fileprivate init(objcBlockList: ObjcBlockList) {
        self.name = objcBlockList.name
        self.source = objcBlockList.source.toBlockListSourceable
        self.dateDownload = objcBlockList.dateDownload
        self.initiator = objcBlockList.initiator.toDownloadInitiator
    }
}

extension Array where Element == BlockList {
    var toObjcBlockLists: [ObjcBlockList] {
        map { $0.toObjcBlockList }
    }
}
