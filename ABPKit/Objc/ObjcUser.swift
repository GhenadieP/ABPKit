/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

// It is needed to define a Objective-C translatable Swift layer that will expose
// the current functionality to Objective-C clients. That is - it was decided
// to create a translatable swift layer rather than modifying existing swift code to
// be Objective-C compatible.
//
// ObjcUser is a lightweight Objective-C convertible representation of User to
// be used to allow Objective-C clients to create work with User instance. It is used just for conversion.
// All of the User related logic still lives in User; ObjcUser will just forward it,
// if needed.

@objcMembers
@objc(User)
public
class ObjcUser: NSObject {
    // MARK: - Public properties -

    /// Current active block list if any
    public var blockList: ObjcBlockList?
    /// Last downloaded blocklist version, if available.
    public var lastVersion: String?

    // MARK: - Internal properties -

    /// The identifier of user
    fileprivate let name: String
    /// Current download list if any
    fileprivate var downloads: [ObjcBlockList]?
    fileprivate var allowlistedDomains: [String]?
    fileprivate var blockListHistory: [ObjcBlockList]?

    init(name: String,
         blockList: ObjcBlockList?,
         blockListHistory: [ObjcBlockList]?,
         downloads: [ObjcBlockList]?,
         lastVersion: String?,
         allowlistedDomains: [String]?) {
        self.name = name
        self.blockList = blockList
        self.blockListHistory = blockListHistory
        self.downloads = downloads
        self.lastVersion = lastVersion
        self.allowlistedDomains = allowlistedDomains
    }

    public override func isEqual(_ object: Any?) -> Bool {
        guard let otherUser = object as? ObjcUser else {
            return false
        }

        func compareDownloads() -> Bool {
            guard let selfDownloads = downloads,
                  let otherDownloads = otherUser.downloads else {
                return false
            }

            return selfDownloads.elementsEqual(otherDownloads,
                                               by: { $0.isEqual($1) })
        }

        func compareHistory() -> Bool {
            guard let blockListHistory = blockListHistory,
                  let otherBlockListHistory = otherUser.blockListHistory else {
                return false
            }

            return blockListHistory.elementsEqual(otherBlockListHistory,
                                                  by: { $0.isEqual($1)})
        }

        func compareBlockList() -> Bool {
            guard let blockList = blockList,
                  let otherBlockList = otherUser.blockList else {
                return false
            }

            return blockList.isEqual(otherBlockList)
        }

        return name == otherUser.name &&
               lastVersion == otherUser.lastVersion &&
               allowlistedDomains == otherUser.getAllowlistedDomains() &&
               compareBlockList() &&
               compareDownloads() &&
               compareHistory()

    }
}

// MARK: - Factory -

extension ObjcUser {
    /// Default init: User gets a default block list.
    @objc(makeInstanceWithError:)
    public
    static func makeInstance() throws -> ObjcUser {
        return try User().toObjcUser
    }

    /// For use during the period where only a single user is supported.
    @objc(makeInstanceFromPersistenceStorage:persistenceID:error:)
    public
    static func makeInstance(fromPersistenceStorage: Bool,
                             persistenceID: String) throws -> ObjcUser {
        return try User(fromPersistentStorage: fromPersistenceStorage,
                        persistenceID: persistenceID).toObjcUser
    }

    /// For use during the period where only a single user is supported.
    @objc(makeInstanceFromPersistenceStorage:error:)
    public
    static func makeInstance(fromPersistenceStorage: Bool) throws -> ObjcUser {
        return try User(fromPersistentStorage: fromPersistenceStorage).toObjcUser
    }

    /// Set the block list during init.
    @objc(makeInstanceFromPersistenceStorage:withBlockList:error:)
    public
    static func makeInstance(fromPersistenceStorage: Bool,
                             withBlockList objcBlockList: ObjcBlockList) throws -> ObjcUser {
        return try User(fromPersistentStorage: fromPersistenceStorage,
                        withBlockList: objcBlockList.toBlockList).toObjcUser
    }

    /// Only a single user is supported here and identifier is not used.
    /// Multiple user support will be in a future version.
    @objc(makeInstancewithPersistenceID:error:)
    public
    static func makeInstance(withPersistenceID persistenceID: String) throws -> ObjcUser {
        return try User(persistenceID: persistenceID).toObjcUser
    }
}

// MARK: - Objc <-> Swift Mapping -

extension User {
    var toObjcUser: ObjcUser {
        ObjcUser(name: name,
                 blockList: blockList?.toObjcBlockList,
                 blockListHistory: blockListHistory?.toObjcBlockLists,
                 downloads: downloads?.toObjcBlockLists,
                 lastVersion: lastVersion,
                 allowlistedDomains: allowlistedDomains)
    }

    init(name: String,
         blockList: BlockList?,
         blockListHistory: [BlockList]?,
         downloads: [BlockList]?,
         lastVersion: String?,
         allowlistedDomains: [String]?) {
        self.name = name
        self.blockList = blockList
        self.blockListHistory = blockListHistory
        self.downloads = downloads
        self.lastVersion = lastVersion
        self.allowlistedDomains = allowlistedDomains
    }
}

extension ObjcUser {
    var toUser: User {
        User(name: name,
             blockList: blockList?.toBlockList,
             blockListHistory: blockListHistory?.toBlockLists,
             downloads: downloads?.toBlockLists,
             lastVersion: lastVersion,
             allowlistedDomains: allowlistedDomains)
    }
}

// MARK: - Getters -

extension ObjcUser {
    public
    func getName() -> String {
        name
    }

    /// Get current active allowlisted domains
    public
    func getAllowlistedDomains() -> [String]? {
        allowlistedDomains
    }

    /// Exclude nil download dates. These are placeholder items that are not added to the store.
    /// This may be need to be revised for integrations that use bundled block lists.
    public
    func getHistory() -> [ObjcBlockList]? {
        toUser.getHistory()?.toObjcBlockLists
    }

    /// Checks if acceptable ads are in use
    public
    func acceptableAdsInUse() -> Bool {
        toUser.acceptableAdsInUse()
    }

    /// Set the given list as currently active and return the new, updated instance
    public
    func withBlockListSet(_ objcBlockList: ObjcBlockList) -> ObjcUser {
        toUser.blockListSet()(objcBlockList.toBlockList).toObjcUser
    }

    /// Gets the current downloads ordered by download date
    public
    func getDownloads() -> [ObjcBlockList]? {
        toUser.getDownloads()?.toObjcBlockLists
    }

    /// Add the given block list to downloads and return the new, updated instance
    public
    func downloadAdded(_ objcBlockList: ObjcBlockList) throws -> ObjcUser {
        try toUser.downloadAdded()(objcBlockList.toBlockList).toObjcUser
    }

    /// Prunes the current downloads to maximum number acceptable and returns a new, updated instance
    public
    func downloadsUpdated() throws -> ObjcUser {
        try toUser.downloadsUpdated().toObjcUser
    }
}

// MARK: - Copiers -

extension ObjcUser {
    /// Set domains for user's allowlist.
    public
    func allowlistedDomainsSet(_ domains: [String]) -> ObjcUser {
        toUser.allowlistedDomainsSet()(domains).toObjcUser
    }

    /// Adds the current blocklist to history while pruning.
    /// Does not automatically get called.
    /// Should be called when changing the user's rule list.
    /// The active BL is kept in the history.
    @objc(historyUpdateWithError:)
    public
    func historyUpdated() throws -> ObjcUser {
        try toUser.historyUpdated().toObjcUser
    }
}

// MARK: - Savers -

extension ObjcUser {
    /// Saves the current state
    @objc(saveWithError:)
    public
    func save() throws {
        try toUser.save()
    }

    /// Saves the current state and returns the current state
    @objc(savedWithError:)
    public
    func saved() throws -> ObjcUser {
        try toUser.saved().toObjcUser
    }
}
