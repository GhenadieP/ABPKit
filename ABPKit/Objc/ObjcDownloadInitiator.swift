/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

// It is needed to define a Objective-C translatable Swift layer that will expose
// the current functionality to Objective-C clients. That is - it was decided
// to create a translatable swift layer rather than modifying existing swift code to
// be Objective-C compatible.
//
// ObjcDownloadInitiator is defined as simple enum with raw type Int, so it can be easily used
// in Objc code, but as soon it needs to be passed deeper into the framework it will be transformed
// to DownloadInitiator type, thus this is used just for conversion and does not contain any logic.

/// Download initiator to be used to as an initiator for a block list
@objc(DownloadInitiator)
public
enum ObjcDownloadInitiator: Int {
    case automaticUpdate
    case repurposedExisting
    case userAction
}

// MARK: - Objc API <-> Swift conversion -

extension ObjcDownloadInitiator {
    init(from downloadInitiator: DownloadInitiator) {
        switch downloadInitiator {
        case .automaticUpdate:
            self = .automaticUpdate
        case .repurposedExisting:
            self = .repurposedExisting
        case .userAction:
            self = .userAction
        }
    }

    var toDownloadInitiator: DownloadInitiator {
        switch self {
        case .automaticUpdate:
            return .automaticUpdate
        case .repurposedExisting:
            return .repurposedExisting
        case .userAction:
            return .userAction
        }
    }
}
