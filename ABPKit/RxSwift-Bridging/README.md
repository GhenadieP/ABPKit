## RxSwift-Bridging

Supports code re-use by mapping common code to the appropriate framework with non-conflicting type aliases or functions based on context.

A corresponding set of files is found in [ABPKit-Combine/RxSwift-Bridging/](../../ABPKit-Combine/RxSwift-Bridging/).
