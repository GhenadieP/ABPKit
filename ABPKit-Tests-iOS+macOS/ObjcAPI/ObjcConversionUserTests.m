/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

#import <XCTest/XCTest.h>
#import <ABPKit/ABPKit-Swift.h>

@interface ObjcConversionUserTests : XCTestCase

@end

@implementation ObjcConversionUserTests

- (void)setUp {
    [super setUp];
    [self removeCachedUser];
}

- (void)tearDown {
    [super tearDown];
    [self removeCachedUser];
}

- (void)test_init_defaultInit_createsCorrectInstance {
    NSError *error;
    User *user = [User makeInstanceWithError:&error];

    BlockList *defaultBlockList = [BlockList makeInstanceWithAcceptableAds:YES
                                                                    source:BlockListSourceRemoteEasylistPlusExceptions
                                                                 initiator:DownloadInitiatorUserAction
                                                                     error:nil];
    XCTAssertNil(error, "Expected to not throw an error");
    XCTAssertNotNil(user.blockList.name, "Expected to have set a blocklist with a name");
    XCTAssertEqual(user.blockList.dateDownload,
                   defaultBlockList.dateDownload,
                   "Expected to init with correct download date");
    XCTAssertEqual(user.blockList.source,
                   defaultBlockList.source,
                   "Expected to init with correct source");
    XCTAssertEqual(user.blockList.initiator,
                   defaultBlockList.initiator,
                   "Expected to init with correct initiator");
    XCTAssertEqualObjects(user.lastVersion,
                          @"0",
                          "Initial version should be zero");
    XCTAssertNotNil([user getName], "Should have a name generated");
    XCTAssertTrue([user getDownloads].count == 0,
                  "Initial downloads should be empty");
    XCTAssertTrue([user getHistory].count == 0,
                  "Initial history should be empty");
    XCTAssertTrue([user getAllowlistedDomains].count == 0,
                  "Initial allowed domains should be empty");
}

- (void)test_initFromPersistence_userNotPersisted_throwsError {
    [self removeCachedUser];
    NSError *error;
    User *savedUser = [User makeInstanceFromPersistenceStorage:YES error:&error];

    XCTAssertNotNil(error, "Expected to throw error when there is no user persisted");
    XCTAssertNil(savedUser, "Expected to not create an instance if no user persisted");
}

- (void)test_initFromPersistence_userExistsInStorage_returnsPersistedUser {
    BlockList *blst = [BlockList makeInstanceWithAcceptableAds:NO
                                                        source:BlockListSourceRemoteEasylist
                                                          name:@"blstName"
                                                  dateDownload:[NSDate date]
                                                     initiator:DownloadInitiatorUserAction
                                                         error:nil];
    User *user = [[User alloc] initWithName:@"name"
                                  blockList: blst
                           blockListHistory:@[blst]
                                  downloads:@[blst]
                                lastVersion:@"1"
                         allowlistedDomains:@[@"domain.com"]];

    NSError *saveError;
    [user saveWithError:&saveError];
    XCTAssertNil(saveError);

    NSError *loadError;
    User *savedUser = [User makeInstanceFromPersistenceStorage:YES error:&loadError];
    XCTAssertNil(loadError);
    XCTAssertTrue([user isEqual:savedUser]);
}

- (void)test_historyUpdated {
    BlockList *blst = [BlockList makeInstanceWithAcceptableAds:NO
                                                        source:BlockListSourceRemoteEasylist
                                                          name:@"blstName"
                                                  dateDownload:[NSDate date]
                                                     initiator:DownloadInitiatorUserAction
                                                         error:nil];
    User *user = [[User alloc] initWithName:@"name"
                                  blockList: blst
                           blockListHistory:@[]
                                  downloads:@[blst]
                                lastVersion:@"1"
                         allowlistedDomains:@[@"domain.com"]];

    NSError *updateError;
    User *updatedUser = [user historyUpdateWithError:&updateError];
    XCTAssertNil(updateError, "Expected no update error");
    XCTAssertTrue([[updatedUser getHistory][0] isEqual:blst],
                  "Expected to add list to history");
}

- (void)test_allowedDomainsAdded {
    User *user = [User makeInstanceWithError:nil];

    NSArray<NSString *> *domains = @[@"domain1.com", @"domain2.com"];
    User *updatedUser = [user allowlistedDomainsSet:domains];
    XCTAssertTrue([updatedUser.getAllowlistedDomains isEqualToArray:domains]);
}

- (void)test_downloadAdded {
    User *user = [User makeInstanceWithError:nil];

    BlockList *blst = [BlockList makeInstanceWithAcceptableAds:NO
                                                        source:BlockListSourceRemoteEasylist
                                                          name:@"blstName"
                                                  dateDownload:[NSDate date]
                                                     initiator:DownloadInitiatorUserAction
                                                         error:nil];

    NSError *updateError;
    User *updateduser = [user downloadAdded:blst error:&updateError];

    // Cannot be tested at the moment.
    // downloadAdded will update the downloadDate to current date,
    // and right now there is no way to control the date
//    XCTAssertTrue([[updateduser getDownloads][0] isEqualTo:blst]);
    XCTAssertNil(updateError, "Expected no update error");
    XCTAssertEqual([updateduser getDownloads].count, 1);
}

- (void)test_blockListSet {
    User *user = [User makeInstanceWithError:nil];

    BlockList *blst = [BlockList makeInstanceWithAcceptableAds:NO
                                                        source:BlockListSourceRemoteEasylist
                                                          name:@"blstName"
                                                  dateDownload:[NSDate date]
                                                     initiator:DownloadInitiatorUserAction
                                                         error:nil];

    User *updatedUser = [user withBlockListSet:blst];
    XCTAssertTrue([updatedUser.blockList isEqual:blst]);
}

- (void)test_acceptableAdsInUse {
    User *user = [User makeInstanceWithError:nil];

    BlockList *aaBlst = [BlockList makeInstanceWithAcceptableAds:YES
                                                        source:BlockListSourceRemoteEasylistPlusExceptions
                                                          name:@"blstName"
                                                  dateDownload:[NSDate date]
                                                     initiator:DownloadInitiatorUserAction
                                                         error:nil];

    User *userWithAA = [user withBlockListSet:aaBlst];
    XCTAssertTrue([userWithAA acceptableAdsInUse]);

    BlockList *nonAABlst = [BlockList makeInstanceWithAcceptableAds:NO
                                                        source:BlockListSourceRemoteEasylist
                                                          name:@"blstName"
                                                  dateDownload:[NSDate date]
                                                     initiator:DownloadInitiatorUserAction
                                                         error:nil];

    User *userWithoutAA = [user withBlockListSet:nonAABlst];
    XCTAssertFalse([userWithoutAA acceptableAdsInUse]);
}

- (void)test_downloadsUpdated {
    BlockList *blst = [BlockList makeInstanceWithAcceptableAds:NO
                                                        source:BlockListSourceRemoteEasylist
                                                          name:@"blstName"
                                                  dateDownload:[NSDate date]
                                                     initiator:DownloadInitiatorUserAction
                                                         error:nil];
    User *user = [[User alloc] initWithName:@"name"
                                  blockList:nil
                           blockListHistory:nil
                                  downloads:@[blst, blst, blst, blst, blst, blst]
                                lastVersion:nil
                         allowlistedDomains:nil];

    NSError *updateError;
    User *updateduser = [user downloadsUpdatedAndReturnError:&updateError];
    XCTAssertNil(updateError, "Expected no update error");
    XCTAssertTrue([updateduser getDownloads].count == 5);
}

// MARK: - Helpers

- (void)removeCachedUser {
    NSString *suiteName;

#if TARGET_OS_OSX
    suiteName = @"group.org.adblockplus.abpkit-macos";
#else
    suiteName = @"group.org.adblockplus.abpkit-ios";
#endif

    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:suiteName];
    [defaults removeObjectForKey:@"user"];
}

@end
