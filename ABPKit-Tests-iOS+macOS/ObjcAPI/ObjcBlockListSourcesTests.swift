/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

@testable import ABPKit
import XCTest

class ObjcBlockListSourcesTests: XCTestCase {

    func test_init_isInitallisedAccordingly() {
        assertIsInitiallisedAccordingly(withSourceable: BundledBlockList.easylist,
                                        expectedState: .bundledEasylist)
        assertIsInitiallisedAccordingly(withSourceable: BundledBlockList.easylistPlusExceptions,
                                        expectedState: .bundledEasylistPlusExceptions)
        assertIsInitiallisedAccordingly(withSourceable: RemoteBlockList.easylist,
                                        expectedState: .remoteEasylist)
        assertIsInitiallisedAccordingly(withSourceable: RemoteBlockList.easylistPlusExceptions,
                                        expectedState: .remoteEasylistPlusExceptions)
    }

    func test_getSourceable_returnsCorrectValue() {
        assertGetSourceableReturnsCorrectValue(from: .bundledEasylist,
                                               expectedSourceable: BundledBlockList.easylist)
        assertGetSourceableReturnsCorrectValue(from: .bundledEasylistPlusExceptions,
                                               expectedSourceable: BundledBlockList.easylistPlusExceptions)
        assertGetSourceableReturnsCorrectValue(from: .remoteEasylist,
                                               expectedSourceable: RemoteBlockList.easylist)
        assertGetSourceableReturnsCorrectValue(from: .remoteEasylistPlusExceptions,
                                               expectedSourceable: RemoteBlockList.easylistPlusExceptions)
    }

    // MARK: - Private

    private func assertIsInitiallisedAccordingly(withSourceable sourceable: BlockListSourceable,
                                                 expectedState: ObjcBlockListSource,
                                                 file: StaticString = #filePath,
                                                 line: UInt = #line) {
        XCTAssertEqual(expectedState,
                       ObjcBlockListSource(sourceable: sourceable))
    }

    private func assertGetSourceableReturnsCorrectValue<S: BlockListSourceable>(from source: ObjcBlockListSource,
                                                                                expectedSourceable: S,
                                                                                file: StaticString = #filePath,
                                                                                line: UInt = #line) where S: Equatable {
        XCTAssertEqual(source.toBlockListSourceable as? S,
                       expectedSourceable)
    }
}
