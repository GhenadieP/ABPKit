/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

@testable import ABPKit
import XCTest

class ObjcDownloadInitiatorTests: XCTestCase {

    func test_init_isInitallisedAccordingly() {
        assertIsInitiallisedAccordingly(withInitiator: .automaticUpdate,
                                        expectedState: .automaticUpdate)
        assertIsInitiallisedAccordingly(withInitiator: .repurposedExisting,
                                        expectedState: .repurposedExisting)
        assertIsInitiallisedAccordingly(withInitiator: .userAction,
                                        expectedState: .userAction)
    }

    func test_getSourceable_returnsCorrectValue() {
        assertGetSourceableReturnsCorrectValue(from: .automaticUpdate,
                                               expectedInitiator: .automaticUpdate)
        assertGetSourceableReturnsCorrectValue(from: .repurposedExisting,
                                               expectedInitiator: .repurposedExisting)
        assertGetSourceableReturnsCorrectValue(from: .userAction,
                                               expectedInitiator: .userAction)
    }

    // MARK: - Private

    private func assertIsInitiallisedAccordingly(withInitiator initiator: DownloadInitiator,
                                                 expectedState: ObjcDownloadInitiator,
                                                 file: StaticString = #filePath,
                                                 line: UInt = #line) {
        XCTAssertEqual(expectedState,
                       ObjcDownloadInitiator(from: initiator))
    }

    private func assertGetSourceableReturnsCorrectValue(from objcInitiator: ObjcDownloadInitiator,
                                                        expectedInitiator: DownloadInitiator,
                                                        file: StaticString = #filePath,
                                                        line: UInt = #line) {
        XCTAssertEqual(objcInitiator.toDownloadInitiator,
                       expectedInitiator)
    }
}
