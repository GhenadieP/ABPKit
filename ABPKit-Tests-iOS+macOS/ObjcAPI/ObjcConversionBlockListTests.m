/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

#import <XCTest/XCTest.h>
#import <ABPKit/ABPKit-Swift.h>

@interface ObjcConversionBlockListTests : XCTestCase

@end

@implementation ObjcConversionBlockListTests


- (void)test_createBlockList_AAMismatch_throwsError {
    [self assertThatBlockListCreationFailsOnAAMismatchWithAAState:YES
                                             forSource:BlockListSourceRemoteEasylist];
    [self assertThatBlockListCreationFailsOnAAMismatchWithAAState:YES
                                             forSource:BlockListSourceBundledEasylist];
    [self assertThatBlockListCreationFailsOnAAMismatchWithAAState:NO
                                             forSource:BlockListSourceRemoteEasylistPlusExceptions];
    [self assertThatBlockListCreationFailsOnAAMismatchWithAAState:NO
                                             forSource:BlockListSourceBundledEasylistPlusExceptions];
}

- (void)test_createBlockList_AAIsValid_createsBlockList {
    [self assertThatBlockListCreationSucceedsWithAAState:NO
                                               forSource:BlockListSourceRemoteEasylist];
    [self assertThatBlockListCreationSucceedsWithAAState:NO
                                               forSource:BlockListSourceBundledEasylist];
    [self assertThatBlockListCreationSucceedsWithAAState:YES
                                               forSource:BlockListSourceRemoteEasylistPlusExceptions];
    [self assertThatBlockListCreationSucceedsWithAAState:YES
                                               forSource:BlockListSourceBundledEasylistPlusExceptions];
}

- (void)test_isExpired_returnsTrueWhenExpired {
    NSError *error = nil;
    NSTimeInterval oneDay = 86400;
    NSDate *expiredDate = [NSDate dateWithTimeInterval:-oneDay
                                             sinceDate:[NSDate date]];
    BlockList *list = [BlockList makeInstanceWithAcceptableAds:NO
                                                        source:BlockListSourceBundledEasylist
                                                          name:@"List"
                                                  dateDownload:expiredDate
                                                     initiator: DownloadInitiatorUserAction
                                                         error:&error];
    XCTAssertTrue([list isExpired],
                  "Expected BlockList to be expired if older than one day");
}

- (void)test_isExpired_returnsFalseWhenNotExpired {
    NSError *error = nil;
    NSDate *notExpiredDate = [NSDate date];
    BlockList *list = [BlockList makeInstanceWithAcceptableAds:NO
                                                        source:BlockListSourceBundledEasylist
                                                          name:@"List"
                                                  dateDownload:notExpiredDate
                                                     initiator: DownloadInitiatorUserAction
                                                         error:&error];
    XCTAssertFalse([list isExpired],
                   "Expected BlockList to not be expired if fresh");
}

- (void)test_initialisation_isPopulatedWithCorrectSource {
    /// BlockListSource gets converted to Swift version and then back, we should assert that this conversion is correct
    [self assertThatBlockListSourceIsCoorectlySetTo:BlockListSourceBundledEasylist
                                            aaState:NO];
    [self assertThatBlockListSourceIsCoorectlySetTo:BlockListSourceBundledEasylistPlusExceptions
                                            aaState:YES];
    [self assertThatBlockListSourceIsCoorectlySetTo:BlockListSourceRemoteEasylist
                                            aaState:NO];
    [self assertThatBlockListSourceIsCoorectlySetTo:BlockListSourceRemoteEasylistPlusExceptions
                                            aaState:YES];
}

- (void)test_initialisation_isPopulatedWithCorrectIniator {
    /// DownloadIniator gets converted to Swift version and then back, we should assert that this conversion is correct
    [self assertThatBlockListDownloadInitiatorIsCoorectlySetTo:DownloadInitiatorUserAction];
    [self assertThatBlockListDownloadInitiatorIsCoorectlySetTo:DownloadInitiatorAutomaticUpdate];
    [self assertThatBlockListDownloadInitiatorIsCoorectlySetTo:DownloadInitiatorRepurposedExisting];
}

// MARK: - Helpers

- (void)assertThatBlockListCreationFailsOnAAMismatchWithAAState:(BOOL)AAstate forSource: (BlockListSource)source {
    NSError *error = nil;
    BlockList *list = [BlockList makeInstanceWithAcceptableAds:AAstate
                                                        source: source
                                                          name:@"name"
                                                  dateDownload: [NSDate date]
                                                     initiator: DownloadInitiatorUserAction
                                                         error:&error];

    XCTAssertNil(list, "Expected creation to fail on AA mismatch");
    XCTAssertNotNil(error, "Expected error to be thrown on AA mismatch");
}

- (void)assertThatBlockListCreationSucceedsWithAAState:(BOOL)AAstate forSource: (BlockListSource)source {
    NSError *error = nil;
    BlockList *list = [BlockList makeInstanceWithAcceptableAds:AAstate
                                                        source:source
                                                     initiator:DownloadInitiatorUserAction
                                                         error:&error];

    XCTAssertNotNil(list, "Expected creation to not fail on AA match");
    XCTAssertNil(error, "Expected error to not be thrown on AA mismatch");
}

- (void)assertThatBlockListSourceIsCoorectlySetTo:(BlockListSource)source aaState:(BOOL)aaState {
    NSError *error = nil;
    BlockList *list = [BlockList makeInstanceWithAcceptableAds:aaState
                                                        source:source
                                                     initiator:DownloadInitiatorUserAction
                                                         error:&error];

    XCTAssertEqual(source, list.source);
}

- (void)assertThatBlockListDownloadInitiatorIsCoorectlySetTo:(DownloadInitiator)initiator {
    NSError *error = nil;
    BlockList *list = [BlockList makeInstanceWithAcceptableAds:NO
                                                        source:BlockListSourceBundledEasylist
                                                     initiator:initiator
                                                         error:&error];

    XCTAssertEqual(initiator, list.initiator);
}

@end
