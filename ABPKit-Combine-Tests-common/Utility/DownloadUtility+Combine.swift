/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

@testable import ABPKit

import Combine
import XCTest

extension DownloadUtility
{
    func downloadForUser(_ lastUser: (Bool) -> User?,
                         afterDownloadTest: (() -> Void)? = nil,
                         afterUserSavedTest: ((User) -> Void)? = nil,
                         withCompleted: (() -> Void)? = nil) -> AnyCancellable
    {
        guard var start = lastUser(true) else { XCTFail("Bad user."); return AnyCancellable(Disposable(dispose: {})) }
        NetworkRequestInterceptor.stubWithShortListSuccessHTTP200Response()
        let dler = BlockListDownloader(user: start, sessionConfig: NetworkRequestInterceptor.sessionConfigForIterception())
        start.persistorProvider = persistorProvider
        dler.persistorProvider = persistorProvider
        return dler.afterDownloads(initiator: .userAction)(dler.userSourceDownloads(initiator: .userAction))
         .sink(receiveCompletion: { cmpl in
            switch cmpl {
            case .finished:
                // May not be reached due to undocumented Combine bug where
                // `.finished` does not propagate from Publishers.FlatMap.
                {}()
            case .failure(let err):
                if err as? ABPCombineError != ABPCombineError.specialFinished {
                    XCTFail(err.localizedDescription)
                }
            }
            log("👩‍🎤started with DLs #\(start.downloads?.count as Int?) - \(start.downloads as [BlockList]?)")
            withCompleted?()
        }, receiveValue: { (usr: User) in
            afterDownloadTest?()
            var copy = usr
            copy.persistorProvider = self.persistorProvider
            do {
                try copy = copy.saved()
            } catch let err { XCTFail("Error: \(err)") }
            afterUserSavedTest?(copy)
        })
    }
}
