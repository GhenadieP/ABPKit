fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## Mac
### mac tests
```
fastlane mac tests
```
Runs local tests on macos platform
### mac swift_host_app_run
```
fastlane mac swift_host_app_run
```
Build and run Swift host app
### mac objc_host_app_run
```
fastlane mac objc_host_app_run
```
Build and run Objective-C host app

----

## iOS
### ios swift_lint
```
fastlane ios swift_lint
```
Gather code quality report
### ios tests
```
fastlane ios tests
```
Runs local tests on ios platform
### ios swift_host_app_run
```
fastlane ios swift_host_app_run
```
Build and run Swift host app
### ios objc_host_app_run
```
fastlane ios objc_host_app_run
```
Build and run Objective-C host app
### ios host_app_release_build
```
fastlane ios host_app_release_build
```
Release build to test that the App can be succesfully exported
### ios public_docs
```
fastlane ios public_docs
```
Generate public docs
### ios coverage_docs
```
fastlane ios coverage_docs
```
Code coverage

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
