# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:mac)

platform :mac do
  
  before_all do |lane|
    prepare_build_operations
  end

  desc "Runs local tests on macos platform"
  lane :tests do
    run_tests(scheme: "ABPKit-macOS")
  end

  desc "Build and run Swift host app"
  lane :swift_host_app_run do
    run_tests(scheme: "HostApp-macOS")
  end

  desc "Build and run Objective-C host app"
  lane :objc_host_app_run do
    xcodebuild(scheme: "ObjC-HostApp-macOS", 
               workspace: "ABPKit.xcworkspace")
  end
end

platform :ios do
  before_all do |lane|
    prepare_build_operations
  end

  desc "Gather code quality report"
  lane :swift_lint do
    swiftlint(output_file: "fastlane/swiftlint.result.json",
              reporter: "json",
              config_file: ".swiftlint.yml")
  end

  desc "Runs local tests on ios platform"
  lane :tests do
    run_tests(scheme: "ABPKit-iOS")
  end

  desc "Build and run Swift host app"
  lane :swift_host_app_run do
    xcodebuild(scheme: "HostApp-iOS", 
               workspace: "ABPKit.xcworkspace")
  end

  desc "Build and run Objective-C host app"
  lane :objc_host_app_run do
    xcodebuild(scheme: "ObjC-HostApp-iOS", 
               workspace: "ABPKit.xcworkspace")
  end

  desc "Release build to test that the App can be succesfully exported"
  lane :host_app_release_build do
    xcodebuild(scheme: "HostApp-iOS", 
               workspace: "ABPKit.xcworkspace", 
               configuration: "Release")
    gym(scheme:"HostApp-iOS",
        workspace: "ABPKit.xcworkspace",
        export_options: "ABPKitDemo/ExportOptions.plist",
        configuration: "Release",
        output_directory: "Build/export")
  end

  desc "Generate public docs"
  lane :public_docs do
    jazzy(config: ".jazzy.yml")
  end
  
  desc "Code coverage"
  lane :coverage_docs do
    slather(proj: "ABPKit.xcodeproj",
            workspace: "ABPKit.xcworkspace",
            scheme: "ABPKit-iOS",
            html:true,
	    output_directory: "public/Coverage_docs")
  end

end

desc "Small preparation before main operations"
private_lane :prepare_build_operations do
  ensure_git_status_clean(show_uncommitted_changes: true, 
                          show_diff: true)
end
