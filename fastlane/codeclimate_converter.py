import json
import sys
import uuid

commitSHA = "undefined"

def slicer(my_str,sub):
   index=my_str.find(sub)
   if index !=-1 :
        return my_str[index:] 
   else :
        raise Exception('Sub string not found!')

def convert(issue):
    ABPKitPath = slicer(issue["file"], "ABPKit/")
    blobSuffix = "/-/blob/" + commitSHA + slicer(ABPKitPath, "/")
    codeClimateItem = {
        'description': issue["reason"],
        'fingerprint': str(uuid.uuid4()),
        'location': {
        'path': "https://gitlab.com/eyeo/adblockplus/ABPKit" + blobSuffix,
        'lines': {
            'begin': issue["line"]
            }
        }
    }

    return codeClimateItem

def main(argv):
    print("[CodeClimate Converter] Running the script")
    with open("fastlane/swiftlint.result.json", "r") as swiftLintData:
        issues = json.load(swiftLintData)

        global commitSHA
        commitSHA = argv[0]
        print("[CodeClimate Converter] Loaded fastlane/swiftlint.json file, converting....")
        convertedIssues = list(map(convert, issues))

        print("[CodeClimate Converter] Converted, writing to file fastlane/swiftlint.json")
        with open("fastlane/swiftlint.result.json", "w") as codeClimateData:
            json.dump(convertedIssues, codeClimateData, indent=4, sort_keys=True)

if __name__ == "__main__":
   main(sys.argv[1:])
