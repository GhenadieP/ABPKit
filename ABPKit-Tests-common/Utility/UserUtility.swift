/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

@testable import ABPKit

import XCTest

class UserUtility {
    let persistorProvider: PersistorProvider

    init(persistorProvider: @escaping PersistorProvider = Persistor.init) {
        self.persistorProvider = persistorProvider
    }

    lazy var lastUser: (Bool) -> User? = {
        do {
            if $0 {
                let persistor = try self.persistorProvider()
                return try persistor.decodeModelData(
                           type: User.self,
                           modelData: persistor.load(type: Data.self, key: ABPMutableState.StateName.user))
            }
            var user = try User()
            user.persistorProvider = self.persistorProvider
            return try user.saved()
        } catch let err { XCTFail("Error: \(err)") }
        return nil
    }

    lazy var aaUserNewSaved: (BlockListSourceable) throws -> User? = {
        var usr = try User(
            fromPersistentStorage: false,
            withBlockList: BlockList(
                withAcceptableAds: true,
                source: $0,
                initiator: .userAction))
        usr.persistorProvider = self.persistorProvider
        return try usr.saved()
    }
}
