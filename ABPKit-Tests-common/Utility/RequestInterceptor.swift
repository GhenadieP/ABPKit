/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

import Foundation

/// Interceptor to be used to intercept requests for testing purpose
class NetworkRequestInterceptor: URLProtocol {
    static var performedRequests = [URLRequest]()

    // MARK: - Stubbing

    private static var stub: Stub?

    struct Stub {
        let data: Data?
        let response: URLResponse?
        let error: Error?
    }

    static func stubResponse(data: Data?, response: URLResponse?, error: Error?) {
        stub = Stub(data: data, response: response, error: error)
    }

    static func stubWithShortListSuccessHTTP200Response() {
        let response = HTTPURLResponse(url: URL(string: "http://some.com")!,
                                       statusCode: 200,
                                       httpVersion: nil,
                                       headerFields: nil)
        stubResponse(data: TestingFileUtility().fileData(resource: "test-v1-easylist-short", ext: ".json")!, response: response, error: nil)
    }

    // MARK: - URLProtocol

    override class func canInit(with request: URLRequest) -> Bool {
        performedRequests.append(request)
        return true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        request
    }

    override func startLoading() {
        /// It is needed to add a small delay due to the fact that that
        /// a download task is started before BlockListDownloader.srcDownloads is set,
        /// thus it happened that the request completed before srcDownloads was and the
        /// download did fail. This is a workouround, to be removed when the issue in blockListDonwloader
        /// will be addressed
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
            if let data = NetworkRequestInterceptor.stub?.data {
                self.client?.urlProtocol(self, didLoad: data)
            }

            if let response = NetworkRequestInterceptor.stub?.response {
                self.client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
            }

            if let error = NetworkRequestInterceptor.stub?.error {
                self.client?.urlProtocol(self, didFailWithError: error)
            }

            self.client?.urlProtocolDidFinishLoading(self)
        }
    }

    override func stopLoading() {}

    // MARK: - Intercept configuration

    static func sessionConfigForIterception() -> URLSessionConfiguration {
        let session = URLSessionConfiguration.ephemeral
        session.protocolClasses?.insert(NetworkRequestInterceptor.self, at: 0)
        startInterceptingRequests()
        return session
    }

    static func startInterceptingRequests() {
        URLProtocol.registerClass(NetworkRequestInterceptor.self)
    }
}
