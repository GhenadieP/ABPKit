/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "TabBarC.h"
#if TARGET_OS_IPHONE
#import "WebViewVC-iOS.h"
#else
#import "WebViewVC-macOS.h"
#endif

@implementation TabBarC : UITabBarController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self tabViewIDsSet];
    NSError *userErr;
    NSError *saveErr;
    [[SetupShared firstUser:false withError:&userErr] saveWithError:&saveErr];
    if (userErr != nil) {
        NSLog(@"🚨 User error: %@", userErr.localizedDescription);
    }
    if (saveErr != nil) {
        NSLog(@"🚨 Save error: %@", saveErr.localizedDescription);
    }
}

- (void)tabViewIDsSet
{
    int cnt = 2;
    NSString *tab = @"tab";
    for(NSUInteger idx = 0; idx < cnt; idx++) {
        ((WebViewVC *)self.viewControllers[idx]).tabViewID = [tab stringByAppendingFormat:@"%lu", idx];
    }
}

@end
