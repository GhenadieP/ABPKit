/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <ABPKit/ABPKit-Swift.h>
#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "SetupShared.h"
#import "WebViewShared.h"
#if TARGET_OS_IPHONE
#import "WebViewVC-iOS.h"
#else
#import "WebViewVC-macOS.h"
#endif

@class WebViewShared;

@interface WebViewVC :
    UIViewController <UITextFieldDelegate,
                      ABPBlockable,
                      WKNavigationDelegate,
                      WKUIDelegate>
{
    BOOL homeInAllowList;
}

@property (weak, nonatomic) IBOutlet UIButton *aaButton;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *reloadButton;
@property (weak, nonatomic) IBOutlet UITextField *urlField;
@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (nonatomic, strong) WebViewShared *shared;
@property (nonatomic, strong) NSString *tabViewID;

- (IBAction)reloadPressed:(id)sender;
- (IBAction)aaPressed:(id)sender;

- (void)reportStatus:(NSString *)status;
- (void)updateAA:(BOOL)aaIsOn;
- (void)updateURLField:(NSString *)urlString;

@end
