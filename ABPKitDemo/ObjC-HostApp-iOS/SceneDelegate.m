/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "SceneDelegate.h"

@interface SceneDelegate ()

@end

@implementation SceneDelegate

#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 130000

// The methods below are placeholders and intentionally empty.

- (void)scene:(UIScene *)scene willConnectToSwession:(UISceneSession *)session options:(UISceneConnectionOptions *)connectionOptions
{

}

- (void)sceneDidDisconnect:(UIScene *)scene
{

}

- (void)sceneDidBecomeActive:(UIScene *)scene
{

}

- (void)sceneWillResignActive:(UIScene *)scene
{

}

- (void)sceneWillEnterForeground:(UIScene *)scene
{

}

- (void)sceneDidEnterBackground:(UIScene *)scene
{

}

#endif

@end
