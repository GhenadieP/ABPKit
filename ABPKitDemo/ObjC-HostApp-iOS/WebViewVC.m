/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "WebViewVC-iOS.h"

@implementation WebViewVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    _shared = [[WebViewShared alloc] initWithWebViewVC:self];
    [self.webView setUIDelegate:self];
    [self.webView setNavigationDelegate:self];
    [self.urlField setDelegate:self];
    if (homeInAllowList) {
        _shared.allowlistedDomains = @[_shared.webHome];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSError *blockerErr;
    NSError *lastUserErr;
    NSError *saveUserErr;
    if (self.shared.abp == nil) {
        [self.shared setAbp:[[ABPWebViewBlocker alloc] initWithHost:self error:&blockerErr]];
    }
    if (blockerErr != nil) {
        NSLog(@"🚨 Blocker error: %@", blockerErr.localizedDescription);
    }
    User *usr = [_shared.abp lastPersistedUserWithError:&lastUserErr];
    if (lastUserErr != nil) {
        NSLog(@"🚨 Last user error: %@", lastUserErr.localizedDescription);
    }
    usr = [[usr allowlistedDomainsSet:_shared.allowlistedDomains] savedWithError:&saveUserErr];
    if (saveUserErr != nil) {
        NSLog(@"🚨 Save user error: %@", saveUserErr.localizedDescription);
    }
    [_shared.abp setUser:usr];
    [self updateAA:usr.acceptableAdsInUse];
    [self disableControls];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_shared setupABP:TRUE withCompletion:^(NSError *err) {
        [self enableControls];
        if (err != nil) {
            NSLog(@"🚨 Setup error: %@", err.localizedDescription);
        }
    }];
}

// ------------------------------------------------------------
// MARK: - Actions -
// ------------------------------------------------------------

- (IBAction)aaPressed:(id)sender
{
    [self disableControls];
    BOOL aaIsOn = FALSE;
    if (_aaButton.titleLabel.text != _shared.aaOn) {
        aaIsOn = TRUE;
    }
    [_shared setupABP:aaIsOn withCompletion:^(NSError *err) {
        [self enableControls];
        if (err != nil) {
            NSLog(@"🚨 Setup error: %@", err.localizedDescription);
        }
    }];
}

- (IBAction)reloadPressed:(id)sender
{
    [_shared loadURLString:[self.urlField text]];
}

// ------------------------------------------------------------

- (void)updateURLField:(NSString *)urlString
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.urlField setText:urlString];
        [self.urlField resignFirstResponder];
    });
}

- (void)reportStatus:(NSString *)status
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.statusLabel setText:status];
        UILabel *lbl = [[UILabel alloc] initWithFrame:self.statusLabel.frame];
        lbl.text = self.statusLabel.text;
        lbl.backgroundColor = self.statusLabel.backgroundColor;
        lbl.font = self.statusLabel.font;
        lbl.textAlignment = self.statusLabel.textAlignment;
        CGRect newFrame = self.statusLabel.frame;
        [lbl sizeToFit];
        newFrame.size.width = lbl.frame.size.width + 16;
        newFrame.origin.x = (self.view.frame.size.width - newFrame.size.width) / 2.0;
        [lbl setFrame:newFrame];
        [self.view addSubview:lbl];
        [lbl setText:status];
        [lbl setAlpha:1.0];
        [lbl setHidden:FALSE];
        [UIView
            animateKeyframesWithDuration:self->_shared.statusDuration
                                   delay:0.0
                                 options:(UIViewKeyframeAnimationOptions)
                                     (UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState)
                              animations:^{
                                  [lbl setAlpha:0.0];
                              }
                              completion:^(BOOL finished) {
                                  [lbl removeFromSuperview];
                              }
        ];
    });
}

- (void)updateAA:(BOOL)aaIsOn
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        if (aaIsOn) {
            [self.aaButton setTitle:self.shared.aaOn forState:UIControlStateNormal];
        } else {
            [self.aaButton setTitle:self.shared.aaOff forState:UIControlStateNormal];
        }
    });
}

- (void)disableControls
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.aaButton setEnabled:FALSE];
        [self.urlField setEnabled:FALSE];
        [self.reloadButton setEnabled:FALSE];
    });
}

- (void)enableControls
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.aaButton setEnabled:TRUE];
        [self.urlField setEnabled:TRUE];
        [self.reloadButton setEnabled:TRUE];
    });
}

// ------------------------------------------------------------
// MARK: - UITextFieldDelegate -
// ------------------------------------------------------------

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (_urlField.text.length > 0) {
        [_shared loadURLString:textField.text];
        return TRUE;
    }
    return FALSE;
}

// ------------------------------------------------------------
// MARK: - WKNavigationDelegate -
// ------------------------------------------------------------

// The following methods are placeholders and intentionally empty.

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
      withError:(NSError *)error
{

}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{

}

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{

}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{

}

@end
