/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <ABPKit/ABPKit-Swift.h>
#import <WebKit/WebKit.h>
#if TARGET_OS_IPHONE
#import "WebViewVC-iOS.h"
#else
#import "WebViewVC-macOS.h"
#endif
#import <Foundation/Foundation.h>

@class WebViewVC;

@interface WebViewShared : NSObject
{
    NSString *webProtocol;
    int retryMax;
    NSTimeInterval retryDelay;
    /// When true, no remote blocklist downloads will be used.
    BOOL noRemote;
    int retryCount;
}

@property (nonatomic, strong) NSString *aaOff;
@property (nonatomic, strong) NSString *aaOn;
/// This message should be displayed whenever new downloaded rules are being used for the first time.
@property (nonatomic, strong) NSString *switchToDLMessage;
///// Length of time to show status messages.
@property NSTimeInterval statusDuration;
@property (nonatomic, strong) NSArray *javascriptDisabledDomains;
/// Set domains to be allowlisted by content blocking. Note that an empty string member ("") will match all domains.
/// If there is a port number (eg 8080), it can be included here with a colon separator.
@property (nonatomic, strong) NSArray *allowlistedDomains;
/// Full URL as a string.
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *webHome;
@property (nonatomic, weak) WebViewVC *wvvc;
@property (nonatomic, strong) ABPWebViewBlocker *abp;

- (instancetype)initWithWebViewVC:(WebViewVC *)wvvc;
- (void)changeUserAA:(BOOL)aaIsOn;
- (void)disableJavaScript:(NSString *)urlString;
- (void)loadURLString:(NSString *)urlString;
- (void)setupABP:(BOOL)aaChangeTo withCompletion:(void (^)(NSError *))completion;
- (void)setupABPForAAIsChanged:(BOOL)aaIsChanged withCompletion:(void (^)(NSError *))completion;
- (void)setupABPWithCompletion:(void (^)(NSError *))completion;

@end
