/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "WebViewShared.h"

@implementation WebViewShared

- (instancetype)initWithWebViewVC:(WebViewVC *)wvvc
{
    self = [super init];
    if (self) {
        self.wvvc = wvvc;
        _allowlistedDomains = [NSMutableArray new];
        _webHome = @"adblockplus.org";
        _aaOff = @"AA is Off";
        _aaOn = @"AA is On";
        _javascriptDisabledDomains = @[@"adblockplus.org"];
        retryDelay = 9.0;
        retryMax = 2;
        webProtocol = @"https";
        _location = [NSString stringWithFormat:@"%@%@%@", webProtocol, @"://", _webHome];
        _statusDuration = 6.0;
        _switchToDLMessage = @"Switched to Downloaded Rules";
    }
    return self;
}

- (void)setupABPWithCompletion:(void (^)(NSError *))completion
{
    [self setupABPForAAIsChanged:FALSE withCompletion:completion];
}

- (void)setupABP:(BOOL)aaChangeTo withCompletion:(void (^)(NSError *))completion
{
    // Set user AA state:
    [self changeUserAA:aaChangeTo];
    NSError *lastUserErr;
    User *usr = [_abp lastPersistedUserWithError:&lastUserErr];
    if (lastUserErr != nil) {
        NSLog(@"🚨 Last user error: %@", lastUserErr.localizedDescription);
    }
    BOOL aaIsChanged = usr.acceptableAdsInUse == aaChangeTo;
    [_wvvc updateAA:usr.acceptableAdsInUse];
    [self setupABPForAAIsChanged:aaIsChanged withCompletion:completion];
}

/// Force compile is a performance option intended to not re-compile existing rules when FALSE.
- (void)setupABPForAAIsChanged:(BOOL)aaIsChanged withCompletion:(void (^)(NSError *))completion
{
    BOOL forceCompile = aaIsChanged;
    [_abp useContentBlockingWithForceCompile:forceCompile logCompileTime:TRUE logBlockListSwitchDL:^{
        [self.wvvc reportStatus:self->_switchToDLMessage];
        NSLog(@"▶️ %@", self->_switchToDLMessage);
    } debugUIUpdate:FALSE completeWith:^(NSError *_Nullable cbError) {
        NSError *lastUserErr;
        User *usr = [self->_abp lastPersistedUserWithError:&lastUserErr];
        if (lastUserErr != nil) {
            NSLog(@"🚨 Last user error: %@", lastUserErr.localizedDescription);
        }
        [self validateRules:usr withError:cbError];
        [self loadURLString:self.location];
        if (cbError != nil) {
            NSLog(@"🚨 Content blocker error: %@", cbError);
        }
        completion(cbError);
     }];
}

- (void)validateRules:(User *)user withError:(NSError *)error
{
    if (error != nil) {
        [_abp validateRulesWithUser:user completion:^(RulesValidation *_Nullable val, NSError *_Nullable err) {
            if (err != nil) {
                NSLog(@"🚨 Rule validation completely failed: %@", err.localizedDescription);
            } else {
                if (val.parseSucceeded) {
                    NSLog(@"⚠️ WK rule loading validation result: %@", val.rulesCounted);
                }
            }
        }];
    }
}

/// Synchronously update user AA state.
- (void)changeUserAA:(BOOL)aaIsOn
{
    BlockListSource src = BlockListSourceRemoteEasylistPlusExceptions;
    NSError *lastUserErr;
    NSError *userBLErr;
    NSError *saveUserErr;
    if (!aaIsOn) {
        src = BlockListSourceRemoteEasylist;
    }
    User *usr = [_abp lastPersistedUserWithError:&lastUserErr];
    if (lastUserErr != nil) {
        NSLog(@"🚨 Last user error: %@", lastUserErr.localizedDescription);
    }
    [usr setBlockList:[BlockList makeInstanceWithAcceptableAds:aaIsOn
                                                        source:src
                                                     initiator:DownloadInitiatorUserAction
                                                         error:&userBLErr]];
    if (userBLErr != nil) {
        NSLog(@"🚨 User set blocklist error: %@", userBLErr.localizedDescription);
    }
    usr = [usr savedWithError:&saveUserErr];
    if (saveUserErr != nil) {
        NSLog(@"🚨 User save error: %@", saveUserErr.localizedDescription);
    }
    [_abp setUser:usr];
}

/// Iterate over JS disabled domains. If the given urlString matches, disable JS.
/// The deprecated setJavaScriptEnabled is used to prevent having to manage new WKWebViews.
- (void)disableJavaScript:(NSString *)urlString
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        for (NSString *dmn in self.javascriptDisabledDomains) {
            if ([[NSURL URLWithString:urlString] host] == [[NSURL URLWithString:dmn] host]) {
                [[[self.wvvc.webView configuration] preferences] setJavaScriptEnabled:FALSE];
            }
        }
    });
}

- (void)loadURLString:(NSString *)urlString
{
    [self disableJavaScript:urlString];
    [_abp loadURLString:urlString completion:^(NSURL *_Nullable url, NSError *_Nullable err) {
        self.location = url.absoluteString;
        [self.wvvc updateURLField:self.location];
    }];
}

@end
