/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "SetupShared.h"

@implementation SetupShared

+ (User *_Nullable)firstUser:(BOOL)useBundledBlocklists withError:(NSError *_Nullable *_Nullable)error;
{
    NSError *__autoreleasing err;
    User *usr = [User makeInstanceWithError:&err];
    if (err != nil) { error = &err; return nil; }
    BlockList *blst;
    if (!useBundledBlocklists) {
        blst = [BlockList makeInstanceWithAcceptableAds:TRUE
                                                 source:BlockListSourceRemoteEasylistPlusExceptions
                                              initiator:DownloadInitiatorUserAction
                                                  error:&err];
    } else {
        blst = [BlockList makeInstanceWithAcceptableAds:TRUE
                                                 source:BlockListSourceBundledEasylistPlusExceptions
                                              initiator:DownloadInitiatorUserAction
                                                  error:&err];
    }
    if (err != nil) { error = &err; return nil; }
    [usr setBlockList:blst];
    return usr;
}

@end
