/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "WebViewVC-macOS.h"

@implementation WebViewVC

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        // Additional setup can go here, if needed.
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _shared = [[WebViewShared alloc] initWithWebViewVC:self];
    [self.webView setUIDelegate:self];
    [self.webView setNavigationDelegate:self];
    [self.urlField setDelegate:self];
    if (homeInAllowList) {
        _shared.allowlistedDomains = @[_shared.webHome];
    }
}

- (void)viewWillAppear
{
    [super viewWillAppear];
    NSError *blockerErr;
    NSError *lastUserErr;
    NSError *saveUserErr;
    if (self.shared.abp == nil) {
        [self.shared setAbp:[[ABPWebViewBlocker alloc] initWithHost:self error:&blockerErr]];
    }
    if (blockerErr != nil) {
        NSLog(@"🚨 Blocker error: %@", blockerErr.localizedDescription);
    }
    User *usr = [_shared.abp lastPersistedUserWithError:&lastUserErr];
    if (lastUserErr != nil) {
        NSLog(@"🚨 Last user error: %@", lastUserErr.localizedDescription);
    }
    usr = [[usr allowlistedDomainsSet:_shared.allowlistedDomains] savedWithError:&saveUserErr];
    if (saveUserErr != nil) {
        NSLog(@"🚨 Save user error: %@", saveUserErr.localizedDescription);
    }
    [_shared.abp setUser:usr];
    [self updateAA:usr.acceptableAdsInUse];
    [self.urlField becomeFirstResponder];
    [self disableControls];
}

- (void)viewDidAppear
{
    [super viewDidAppear];
    [_shared setupABPWithCompletion:^(NSError *err) {
        [self enableControls];
        if (err != nil) {
            NSLog(@"🚨 Setup error: %@", err.localizedDescription);
        }
    }];
}

// ------------------------------------------------------------
// MARK: - Actions -
// ------------------------------------------------------------

- (IBAction)enterURLSelected:(id)sender
{
    [self.view.window makeFirstResponder:self.urlField];
}

- (IBAction)aaPressed:(id)sender
{
    [self disableControls];
    BOOL aaIsOn = FALSE;
    if (_aaCheckButton.state == NSControlStateValueOn) {
        aaIsOn = TRUE;
    }
    [_shared setupABP:aaIsOn withCompletion:^(NSError *err) {
        [self enableControls];
        if (err != nil) {
            NSLog(@"🚨 Setup error: %@", err.localizedDescription);
        }
    }];
}

- (IBAction)reloadPressed:(id)sender
{
    [_shared loadURLString:[self.urlField stringValue]];
}

// ------------------------------------------------------------

- (void)updateURLField:(NSString *)urlString
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.urlField setStringValue:urlString];
    });
}

- (void)reportStatus:(NSString *)status
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.statusField setStringValue:status];
        [self.statusField setHidden:FALSE];
        [NSAnimationContext runAnimationGroup:^(NSAnimationContext *_Nonnull context) {
            [context setDuration:0.0];
            [[self.statusField animator] setAlphaValue:1.0];
        } completionHandler:^{
            [NSAnimationContext runAnimationGroup:^(NSAnimationContext *_Nonnull context) {
                [context setDuration:self->_shared.statusDuration];
                [self.statusField.animator setAlphaValue:0.0];
            }];
        }];
    });
}

- (void)updateAA:(BOOL)aaIsOn
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        if (aaIsOn) {
            [self->_aaCheckButton setState:NSControlStateValueOn];
        } else {
            [self->_aaCheckButton setState:NSControlStateValueOff];
        }
    });
}

- (void)disableControls
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.aaCheckButton setEnabled:FALSE];
        [self.urlField setEnabled:FALSE];
        [self.reloadButton setEnabled:FALSE];
    });
}

- (void)enableControls
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.aaCheckButton setEnabled:TRUE];
        [self.urlField setEnabled:TRUE];
        [self.reloadButton setEnabled:TRUE];
        [self.urlField becomeFirstResponder];
    });
}

// ------------------------------------------------------------
// MARK: - NSTextFieldDelegate -
// ------------------------------------------------------------

- (void)controlTextDidEndEditing:(NSNotification *)obj
{
    if ([[_urlField stringValue] length] > 0) {
        [_shared loadURLString:_urlField.stringValue];
    }
}

// ------------------------------------------------------------
// MARK: - WKNavigationDelegate -
// ------------------------------------------------------------

// The following methods are placeholders and intentionally empty.

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
      withError:(NSError *)error
{

}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    
}

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
    
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    
}

@end
