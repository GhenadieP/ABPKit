/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "TabVC.h"

@implementation TabVC

/// First user is created. Alternatively, ABPKit can start with a persisted User state but this is
/// not the recommended usage for clients.
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSError *userErr;
    NSError *saveErr;
    [[SetupShared firstUser:false withError:&userErr] saveWithError:&saveErr];
    if (userErr != nil) {
        NSLog(@"🚨 User error: %@", userErr.localizedDescription);
    }
    if (saveErr != nil) {
        NSLog(@"🚨 Save error: %@", saveErr.localizedDescription);
    }
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        height = 744;
        width = 1024;
        xPosition = 50;
        yPosition = 600;
    }
    return self;
}

- (void)viewWillAppear
{
    [super viewWillAppear];
    [self.view.window setFrame:CGRectMake(xPosition, yPosition, width, height) display:YES];
}

- (void)tabView:(NSTabView *)tabView didSelectTabViewItem:(NSTabViewItem *)tabViewItem
{
    [super tabView:tabView didSelectTabViewItem:tabViewItem];
    if (tabViewItem != nil) {
        ((WebViewVC *)tabViewItem.viewController).tabViewID = (NSString *)tabViewItem.identifier;
    } else {
        ((WebViewVC *)tabViewItem.viewController).tabViewID = @"";
    }
}

@end
