ABPKit
======
WebKit content blocker management framework for iOS and macOS supporting both Safari and WKWebView.

## History
This project serves the future of content blocking on iOS and macOS after evolving out of the ABP for Safari iOS app. Built upon a newly created functional and reactive Swift architecture, it employs RxSwift/Combine for mastery over asynchronous operations. State handling is through an immutable model conforming to Codable with persistence to local storage. Remaining legacy dependencies are intended to be resolved with further updates.

- ABPKit (based on RxSwift) is available.
- ABPKit-Combine is under active development and not yet suitable for production use.

## Features
- Non-blocking operations are directed by an immutable user state, preventing the need for locks or additional event observers to maintain consistency
- Example [host apps](#host-apps) for iOS and macOS are provided as a separate project in a workspace
- Automatic downloading of remote block list sources when needed
- Option for [bundled block lists](#bundled-block-lists-config) to be used before downloads
- New block list sources are activated as soon as they are ready
- Rule lists and downloads are synchronized (or pruned) according to user state
- Automatic WebKit rule compilation and removal, to prevent accumulation
- Persistable user state to local storage
- Allowlistable domain names for users
- Automatic [periodic block list updates](#periodic-block-list-updates) that can be temporarily suspended

## Installation
Tested with Xcode 12, RxSwift 5, iOS (11/12/13/14) and macOS 10.15 (Catalina).

### Building
- `git clone git@gitlab.com:eyeo/adblockplus/abpkit.git`
- `brew install carthage`
- `brew install swiftlint` (optional)
- `cd abpkit`
- `cp Cartfile.binary Cartfile` (optional)
- `carthage update --platform "ios,macos"` or `./Scripts/carthage_update_xcode_12.sh`
- `open ABPKit.xcworkspace`
- Choose a scheme and build it in Xcode

The ABPKit framework depends only on RxSwift (beyond the iOS or macOS SDKs). RxRelay and RxBlocking only apply to the testing targets. Switching between ABPKit-Combine and RxSwift-based ABPKit targets may require a clean build to properly resolve symbols.

### Testing
Included unit tests verify correct results for many usages.

Additionally, mutable state handling can be verified through UI testing where automatic updates, the active tab, and the selected block list can be rapidly changed, continuously. This testing is not intended for general use but see [HostAppTester.swift](ABPKitDemo/HostApp-common/HostAppTester.swift) for details.

## Manage ABPKit with Carthage in your project

The legacy build system is required to build with Carthage. Otherwise, the new build system may be used.

### Preparing ABPKit in your project
Add this line to your `Cartfile`:
```
git "https://gitlab.com/eyeo/adblockplus/abpkit" ~> 0.5
```
### Building ABPKit in your project
Run:

- `carthage update --platform "ios,macos"`

Built frameworks will be in `Carthage/Build/`. Drag them into Xcode, as needed.

### Xcode 12 and Carthage
When validating your app with the App Store, Carthage builds can include unsupported architectures. The script at [`./Scripts/remove_abpkit_and_rxswift_unused_architectures.sh`](./Scripts/remove_abpkit_and_rxswift_unused_architectures.sh) removes them when installed as a build phase script.

### Configuration of your project
<a name="app-groups"></a>
#### 1. App groups
App groups are required on iOS and macOS to identify local data stores for the framework. Please
copy the property list named [`ABPKit-Configuration.plist`](ABPKit-Configuration.plist) to
your main bundle and set values for the keys `appGroupIOS` and `appGroupMacOS` to match one of
your active entitlements where a value, but not the key, may be empty if the platform will not
be used. This file should be copied into _your project's main bundle_ during a build copy phase.

<a name="bundled-block-lists-config"></a>
#### 2. Bundled block lists
By default, bundled block lists will not be copied to the built framework and, therefore, will not be available at runtime. To have them accessible to ABPKit for your app, set the value of `installBundledBlockLists` in `ABPKit-BundledBlockLists.plist` to `YES` and they will be copied at build time. See [bundled block lists usage](#bundled-block-lists-usage) for more information.

#### 3. Partners
- The copy of the property list named `ABPKit-Configuration.plist` linked in [(1) App
groups](#app-groups), also provides configuration of essential key-value pairs used to
identify your integration while downloading filter lists. It is vital to update these
values with every release of your product in order to match the newest name/version.
- Please adjust `partnerApplication` to match the name of your product and adjust
`partnerApplicationVersion` to match the version of your product. For example, set key
`partnerApplication` to `CustomBrowser` and key `applicationVersion` to `1.5.11a`.
- Please only adjust `addonName` after consulting with eyeo.

<a name="figure-1"></a>
---
![Host app content blocking example](./Documentation/Images/cb-testing-example.png)
---
Figure 1: Host app content blocking example. The highlighted element (shown) will be hidden when AA enabled is false.

<a name="host-apps"></a>
## Host app demonstrations
Example apps for iOS and macOS are included in the project `ABPKitDemo` to demonstrate some of the usages of ABPKit. The status of content blocking can be observed in the console and through the UI. Downloaded rules are not active until the message "Switched to Downloaded Rules" is displayed.

Since display of ads is not reliable in the host apps due to them not offering a full web browsing environment recognized by advertisers, debug builds have a blocking rule injected to verify content blocking via AA switching. Their default home page, adblockplus.org, is used as a test subject as illustrated in Figure 1. For the purpose of testing only, JavaScript is disabled for the domain.

- When AA is enabled, an exception rule is enabled and content is not blocked.
- When AA is disabled, the image (see [Figure 1](#figure-1)) matching the selector in the [debugging rule](ABPKit/ContentBlocking/WKWebView/WebKitContentBlocker+Debugging.swift) is hidden.

## Framework code examples
Integration of ABPKit is not intended to be possible by copy-and-pasting a series of code snippets. Rather, the following code samples illustrate some of the different contexts where calls to the framework may apply. The foundation of all operations is the User state model and having a clear understanding of its precise usage is the key to getting correct results.

### User state initialization
The data model that mediates all operations is based on immutable data structures that are persisted to local storage. An initial state is required to be stored to initialize the persistence layer. For a default user state, this can be done with the following code:

* Swift:

```swift
try User().save()
```

* Objective-C:
```objective-c
NSError *error;
[user saveWithError:&error];
```

This stored User allows for sharing user state across [multiple web views](#multiple-web-views). Therefore, the initial state _should be performed outside_ of any web view instance. There is an example in the demo host app under `changeUserAA()` in [SetupShared.swift](ABPKitDemo/HostApp-common/SetupShared.swift).

### Content blocking in a WKWebView

* Swift

```swift
import ABPKit

class WebViewVC: ABPBlockable
{
    var abp: ABPWebViewBlocker!
    var webView: WKWebView!

    override func viewDidLoad()
    {
        do { abp = try ABPWebViewBlocker(host: self) }
        catch let err { } // Handle any error inside the catch block
    }
}
```

* Objective-C

```objective-c
#import "WebViewVC.h"
#import <ABPKit/ABPKit-Swift.h>

@interface WebViewVC () <ABPBlockable>
@property(nonatomic, strong) WKWebView *webView;
@property(nonatomic, strong) ABPWebViewBlocker *abp;
@end

@implementation WebViewVC

- (void)viewDidLoad {
    NSError *error;
    abp = [[ABPWebViewBlocker alloc] initWithHost:self error:&error];
    /// Handle error
}

@end
```

### Allowlisting

* Swift
```swift
// Set
abp.user = abp.user.allowlistedDomainsSet()(["example.com"]).saved()

// Get
let domains = abp.user.getAllowlistedDomains()
```

* Objective-C

```objective-c
// Set
NSError *error;
User *savedUser = [[[_abp getUser] allowlistedDomainsSet:@[@"example.com"]] savedWithError:&error]
// Check error and set if success
[_abp setUser: savedUser];

// Get
NSArray<*NSString> *domains = [[_abp getUser] getAllowlistedDomains];
```


### Use content blocking sources automatically

* Swift 

```swift
// With the last saved user state:
abp.useContentBlocking(completeWith:
{
    // Code to load URL
})
```

* Objective-C

```objective-c
// With the last saved user state:
[_abp useContentBlockingWithCompletion:^(NSError * _Nullable) {
   // Code to load URL
}];
```

Downloading of a block list is initiated, if needed.

<a name="persistence"></a>
### User state persistence
Many operations are controlled by the last saved user state. The following code shows how a User may be stored and retrieved. The demonstration host apps provide a concrete overview of how user states can be used in an app.

* Swift

```swift
// Persist user state while replacing any existing version
try abp.user.save()

// Persist user state while replacing any existing version and return a copy
let saved = try abp.user.saved()

// Retrieve the last persisted state
let user = try User(fromPersistentStorage: true)
```
or
```swift
let user = try abp.lastUser()
```

* Objective-C

```objective-c
// Persist user state while replacing any existing version
[[_abp getUser] saveWithError:&error];

// Persist user state while replacing any existing version and return a copy
User *user = [[_abp getUser] savedWithError:&error];

// Retrieve the last persisted state
User *user = [User makeInstanceFromPersistenceStorage:true error:&error];
```
or
```objective-c
User *user = [_abp lastPersistedUserWithError:&error];
```

### Enable/Disable Acceptable Ads (AA)

* Swift

```swift
// Enable
abp.user = try abp.user.blockListSet()(BlockList(
    withAcceptableAds: true,
    source: RemoteBlockList.easylistPlusExceptions,
    initiator: .userAction))

// Disable
abp.user = try abp.user.blockListSet()(BlockList(
    withAcceptableAds: false,
    source: RemoteBlockList.easylist,
    initiator: .userAction))
```

* Objective-C

```objective-c
// Enable
BlockList *blockList = [BlockList makeInstanceWithAcceptableAds:true
                                                         source:BlockListSourceRemoteEasylistPlusExceptions
                                                      initiator:DownloadInitiatorUserAction
                                                          error:&error];
User *user = [[_abp getUser] withBlockListSet:blockList];
[_abp setUser: user];

// Disable
BlockList *blockList = [BlockList makeInstanceWithAcceptableAds:false
                                                         source:BlockListSourceRemoteEasylistPlusExceptions
                                                      initiator:DownloadInitiatorUserAction
                                                          error:&error];
User *user = [[_abp getUser] withBlockListSet:blockList];
[_abp setUser: user];
```

The new block list can be activated with `useContentBlocking(completeWith:)` for Swift or `useContentBlockingWithCompletion:` for Objective-C

### Verify AA usage

* Swift

```swift
let aaInUse = abp.user.acceptableAdsInUse()
```

* Objective-C

```objective-c
BOOL aaInUse = [[_abp getUser] acceptableAdsInUse];
```

<a name="bundled-block-lists-usage"></a>
### Bundled block lists
User state can be set to use bundled block lists before downloads. When this is the first state during `useContentBlocking(completeWith:)` for Swift or `useContentBlockingWithCompletion:` for Objective-C, bundled lists will be used before attempting to download remote versions. See `firstUser(useBundledBlocklists:)` for an example.

* Swift

```swift
abp.user = try abp.user.blockListSet()(BlockList(
    withAcceptableAds: true,
    source: BundledBlockList.easylistPlusExceptions,
    initiator: .userAction))
```

* Objective-C

```objective-c
// Enable
BlockList *blockList = [BlockList makeInstanceWithAcceptableAds:true
                                                         source:BlockListSourceBundledEasylistPlustExceptions
                                                      initiator:DownloadInitiatorUserAction
                                                          error:&error];
User *user = [[_abp getUser] withBlockListSet:blockList];
[_abp setUser: user];
```

<a name="multiple-web-views"></a>
### Multiple web views
State changes to be shared among web views should be persisted and then loaded, as needed (see [User state persistence](#persistence)). The host apps, for iOS and macOS, provide an example of this state handling across two separate views embedded in tabs. The initial user state is created in each app's respective tab view controller. The last user state is then loaded when a tab is selected.

### Re-using downloads
Downloading and compiling new block list rules is not always what is wanted. For example, if an app is creating multiple web views, it is sufficient to have a block list processed once for a user and then re-used across additional web views. This requires managing the user state such that a `BlockList` struct corresponding to rules that were previously downloaded is passed to `ABPWebViewBlocker` where needed. See [WebViewShared.swift](ABPKitDemo/HostApp-Common/WebViewShared.swift) for an example. 

<a name="periodic-block-list-updates"></a>
### Periodic block list updates
Updates of the user's active block list are performed automatically for every 24-hour period when the host app is open. The expiration interval is based on the last download and only one update will happen per day, if needed. The update is based on the last downloaded block list. This behavior can be overridden for situations where a download may not be suitable such as low-bandwidth network conditions. Overriding the updates can be accomplished with a single function that returns the user to be updated.

#### Overriding the user state
If you want to override the user state used for updates, assign a function of type `() -> User?` to `ABPBlockListUpdater.sharedInstance().userForUpdates`. An example that will prevent updates from happening is shown below.

* Swift

```swift
// Prevent user updates.
ABPBlockListUpdater.sharedInstance().userForUpdates = { () -> User? in return nil }
```

* Objective-C

```objective-c
// Prevent user updates.
[[ABPBlockListUpdater sharedInstance] setUserForUpdate:nil];
```

Restoring the original behavior can be accomplished with:

* Swift

```swift
// Enable default user updates based on the last persisted user state.
ABPBlockListUpdater.sharedInstance().userForUpdates = ABPBlockListUpdater.defaultUserForUpdates
```

* Objective-C

```objective-c
// Enable default user updates based on the last persisted user state.
[[ABPBlockListUpdater sharedInstance] setUserForUpdate:[[ABPBlockListUpdater sharedInstance] getDefaultUserForUpdate]];
```

The function `userForUpdates` may be assigned at any time after an `ABPWebViewBlocker` has been created.

### Content Blocking on Safari for macOS
- Choose scheme `HostApp-macOS`
- Run in Xcode

The extension `HostCBExt-macOS` is installed to Safari. Content blocking in Safari can be activated by enabling the extension.

### Content Blocking on Safari for iOS
Not yet implemented.

## Release notes
Please see the [change log](CHANGELOG.md) for release notes.

## License
ABPKit is released as open source software under the GPL v3 license, see the [license file](LICENSE.md) in the project root for the full license text.
