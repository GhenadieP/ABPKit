/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

import Combine

extension BlockListDownloader
{
    /// - returns: State of a User or Updater to be used for downloading.
    // swiftlint:disable force_cast
    func afterDownloads<S>(initiator: DownloadInitiator) -> (StreamDownloadEvent) -> AnyPublisher<S, Error>
    where S: BlockListDownloadable & Persistable
    {
        { event in
            event
                .last()
                .filter { $0.didFinishDownloading == true }
                .flatMap { [unowned self] _ -> AnyPublisher<S, Error> in
                    do {
                        try self.sessionInvalidate()
                    } catch let err { return Fail(error: err).eraseToAnyPublisher() }
                    switch initiator {
                    case .userAction:
                        return SinglePublisher(self.user as! S).eraseToAnyPublisher()
                    case .automaticUpdate:
                        return SinglePublisher(self.updater as! S).eraseToAnyPublisher()
                    default:
                        return Fail(error: ABPBlockListError.badInitiator).eraseToAnyPublisher()
                    }
                }.eraseToAnyPublisher()
        }
    }
    // swiftlint:enable force_cast

    /// Performs downloading and assigns events.
    /// - returns: Observable of all concatenated user download events.
    func userSourceDownloads(initiator: DownloadInitiator) -> StreamDownloadEvent
    {
        do {
            // Downloader has state dependency on source DLs property:
            switch initiator {
            case .userAction:
                srcDownloads = try blockListDownloadsForUser(initiator: initiator)(user)
            case .automaticUpdate:
                srcDownloads = try blockListDownloadsForUser(initiator: initiator)(updater)
            default:
                return Fail(error: ABPBlockListError.badInitiator).eraseToAnyPublisher()
            }
            // Downloader has a state dependency on download events:
            downloadEvents = makeDownloadEvents()(srcDownloads)
            return downloadEvents.map { $1 }.concat() ?? Empty().eraseToAnyPublisher()
        } catch { return Fail(error: ABPUserModelError.badDownloads).eraseToAnyPublisher() }
    }

    /// Seed events.
    func makeDownloadEvents() -> ([SourceDownload]) -> TaskDownloadEvents
    {
        {
            Dictionary(uniqueKeysWithValues: $0
                .map { $0.task?.taskIdentifier }
                .compactMap { ($0!, CurrentValueSubject<DownloadEvent, Error>(DownloadEvent())) })
        }
    }
}
