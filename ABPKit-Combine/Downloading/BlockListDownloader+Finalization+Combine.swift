/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

import Combine

extension BlockListDownloader
{
    func moveItemAction() -> (MoveItemData) -> Void
    {
        { dat in
            self.finalizeCancellable = self.moveItem(
                    source: dat.source,
                    destination: dat.destination)
                .sink(receiveCompletion: { cmpl in
                    switch cmpl {
                    case .failure(let err):
                        self.reportError(err, taskID: dat.taskID)
                    case .finished:
                        self.moveItemComplete()(dat)
                    }
                }, receiveValue: { _ in }) // source is Never
        }
    }

    private
    func moveItem(source: URL, destination: URL?) -> CompleteOnly
    {
        guard let dst = destination else { return Fail(error: ABPDownloadTaskError.badDestinationURL).eraseToAnyPublisher() }
        do {
            try FileManager.default.moveItem(at: source, to: dst)
            return Empty().eraseToAnyPublisher()
        } catch let err { return Fail(error: err).eraseToAnyPublisher() }
    }
}
