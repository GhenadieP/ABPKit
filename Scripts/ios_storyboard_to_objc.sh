#!/bin/sh
#
# Make changes needed to use the storyboard with an Objective-C host app.

PATH="ABPKitDemo/HostApp-iOS-UI-common/Base.lproj"
USR_BIN=/usr/bin
BIN=/bin
FILE="Main.storyboard"
SUFFIX="ios_swift"

if [ ! -f "$PATH/$FILE.$SUFFIX" ]; then
	$BIN/cp "$PATH/$FILE" "$PATH/$FILE.$SUFFIX"
fi 

$USR_BIN/sed -i .bak 's/customClass=\"\(TabBarC\)\" customModule=\"HostApp_iOS\" customModuleProvider=\"target\" sceneMemberID=\"viewController\"/customClass=\"\1\" sceneMemberID=\"viewController\"/g' $PATH/Main.storyboard
$USR_BIN/sed -i .bak 's/customClass=\"\(WebViewVC\)\" customModule=\"HostApp_iOS\" customModuleProvider=\"target\" sceneMemberID=\"viewController\"/customClass=\"\1\" sceneMemberID=\"viewController\"/g' $PATH/Main.storyboard
