#!/bin/bash 
#
# Revert to the original storyboard intended for Swift.

PATH="ABPKitDemo/HostApp-iOS-UI-common/Base.lproj"
BIN=/bin
FILE="Main.storyboard"
ORIG="Main.storyboard.ios_swift"

if [ -f "$PATH/$ORIG" ]; then
	$BIN/mv $PATH/$ORIG $PATH/$FILE
else 
	echo "Warning: $PATH/$ORIG not found."
fi

if [ -f "$PATH/$FILE.bak" ]; then
	$BIN/rm $PATH/$FILE.bak
else 
	echo "Warning: $PATH/$FILE.bak not found."
fi
