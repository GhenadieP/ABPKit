#!/bin/sh

# Update bundled block lists in ABPKit.
#
# Run from the project root with:
#
# $ Scripts/update-bundled-blocklists.sh

source "Scripts/blocklists-default.txt"
curl -o "$LOCAL_EASYLIST" "$REMOTE_EASYLIST" --cacert Scripts/easylist-downloads.adblockplus.org.chained.crt
curl -o "$LOCAL_EASYLIST_PLUS_EXCEPTIONS" "$REMOTE_EASYLIST_PLUS_EXCEPTIONS" --cacert Scripts/easylist-downloads.adblockplus.org.chained.crt

if [ -f $LOCAL_EASYLIST ]
then
    cp "$LOCAL_EASYLIST" "$CB_OUTPUT_PATH"
fi

if [ -f $LOCAL_EASYLIST_PLUS_EXCEPTIONS ]
then
    cp "$LOCAL_EASYLIST_PLUS_EXCEPTIONS" "$CB_OUTPUT_PATH"
fi
