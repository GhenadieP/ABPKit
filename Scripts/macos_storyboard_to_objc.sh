#!/bin/sh
#
# Make changes needed to use the storyboard with an Objective-C host app.

PATH="ABPKitDemo/HostApp-macOS-UI-common/Base.lproj"
USR_BIN=/usr/bin
BIN=/bin
FILE="Main.storyboard"
SUFFIX="macos_swift"

if [ ! -f "$PATH/$FILE.$SUFFIX" ]; then
	$BIN/cp "$PATH/$FILE" "$PATH/$FILE.$SUFFIX"
fi 


$USR_BIN/sed -i .bak 's/customClass=\"\(AppDelegate\)\" customModule=\"HostApp_macOS\" customModuleProvider=\"target\"/customClass=\"\1\"/g' $PATH/$FILE
$USR_BIN/sed -i .bak 's/customClass=\"\(TabVC\)\" customModule=\"HostApp_macOS\" customModuleProvider=\"target\"/customClass=\"\1\"/g' $PATH/$FILE
$USR_BIN/sed -i .bak 's/customClass=\"\(WebViewVC\)\" customModule=\"HostApp_macOS\" customModuleProvider=\"target\"/customClass=\"\1\"/g' $PATH/$FILE
